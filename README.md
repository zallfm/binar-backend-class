# Resources
Resources for learning, open contribute :

### Pre
-  [Data Structure and Algorithm](https://www.geeksforgeeks.org/data-structures/)

### Software Design Principles
-   [S.O.L.I.D principles](https://robots.thoughtbot.com/back-to-basics-solid)
-   [Software Arcitecture](https://sourcemaking.com/)

### Git
-   [Book Git](https://books.goalkicker.com/GitBook/)
-   [Git Intro](https://git-scm.com/book/id/v1/Memulai-Git-Tentang-Version-Control)

### Ruby
-   [Book Ruby Basic](https://launchschool.com/books/ruby/read/introduction)
-   [Book Ruby OOP(Object Oriented Programming)](https://launchschool.com/books/oo_ruby/read/introduction)
-   [Ruby Code Convention](https://github.com/rubocop-hq/ruby-style-guide)
-   [Ruby Monk](https://rubymonk.com/)
-   [Hackerrank Ruby](https://www.hackerrank.com/domains/ruby)
-   [Codecademy Ruby](https://www.codecademy.com/learn/learn-ruby)
-   [Awesome Ruby Project](http://awesome-ruby.com/)
-   [IdRails](http://www.idrails.com/series)
-   [Railscasts](http://railscasts.com/)

### Ruby on Rails
-   [Ruby on Rails Book](https://www.railstutorial.org/book)
-   [Ruby on rails api](https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-one)

## Postgresql
-   [Book Postgresql](https://books.goalkicker.com/PostgreSQLBook/)
-   [Setup Postgresql](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04)

## Deploy
-   [Deploy Heroku](https://devcenter.heroku.com/articles/getting-started-with-rails5)

## CI/CD
- [.gitlab-ci.yml examples](https://docs.gitlab.com/ee/ci/examples/)
