# Resources
Resources for learning Javascript, open contribute :

### Javascript Basics
-  [W3schools](https://www.w3schools.com/js/default.asp)
-  [Codecademy](https://www.codecademy.com/learn/introduction-to-javascript)
-  [Egghead](https://egghead.io/search?query=javascript)